const http = require("http");
const express = require("express");
const cors = require("cors");
const SimpleWebRTC = require("simplewebrtc");
const wrtc = require("wrtc");

const app = express();
const server = http.createServer(app);

const PORT = 3001;

app.use(cors());

const webrtc = new SimpleWebRTC({
  wrtc,
  server: { httpServer: server, autoAcceptConnections: false },
});

app.get("/", (req, res) => {
  res.send("WebRTC Signaling Server is running!");
});

server.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}`);
});

webrtc.on("connection", (peer) => {
  console.log("Peer connected:", peer.id);
});

webrtc.on("message", (data, connection) => {
  console.log("Received message from", connection.id, ":", data);
});

// global.navigator.geolocation.getCurrentPosition((data) => {
//   console.log(data);
// });
