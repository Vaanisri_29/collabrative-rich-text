import "./App.css";
import RichTextEditor from "./component/RichTextEditor";

function App() {
  return (
    <div
      style={{
        textAlign: "center",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <h1>Collaborative Rich Text Editor</h1>
      <RichTextEditor />
    </div>
  );
}

export default App;
