import React, { useState } from "react";
import * as Y from "yjs";
import ReactQuill, { Quill } from "react-quill";
import "react-quill/dist/quill.snow.css";
import "react-quill-emoji/dist/quill-emoji.css";
import quillEmoji from "react-quill-emoji";
import "quill-mention";
import QuillCursors from "quill-cursors";
import "quill-paste-smart";
import * as Emoji from "quill-emoji";
import Focus from "quill-focus";

Quill.register(
  {
    "modules/emoji-shortname": quillEmoji.ShortNameEmoji,
    "modules/cursors": QuillCursors,
    "modules/emoji": Emoji,
    "modules/focus": Focus,
  },
  true
);

function RichTextEditor() {
  let quillRef = null;
  let reactQuillRef = null;

  const [value, setValue] = useState({
    text: "",
    received: true,
    focus: "initial",
  });
  console.log("value in state::", value);

  React.useEffect(() => {
    attachQuillRefs();

    const ydoc = new Y.Doc();
    // const provider = new WebrtcProvider(
    //   "http://localhost:3001",
    //   "quill-demo",
    //   ydoc
    // );
    const ytext = ydoc.getText("quill");

    // const binding = new QuillBinding(ytext, quillRef);
    // return () => {
    //   provider.disconnect();
    // };
  }, []);

  const attachQuillRefs = () => {
    if (typeof reactQuillRef.getEditor !== "function") return;
    quillRef = reactQuillRef.getEditor();
  };
  const toolbarOptions = {
    container: [
      [{ header: [1, 2, 3, 4, 5, 6, false] }],
      ["bold", "italic", "underline", "strike", "blockquote"],
      ["emoji"],

      [
        { align: [] },
        { script: "sub" },
        { script: "super" },
        { list: "ordered" },
        { list: "bullet" },
        { indent: "-1" },
        { indent: "+1" },
      ],
      ["formula", "table"],
      ["link", "image"],
      ["clean"],
    ],
    // handlers: {
    //   emoji: function () {},
    // },
  };

  const modulesRef = {
    toolbar: toolbarOptions,
    "emoji-toolbar": true,
    "emoji-shortname": true,
    "emoji-textarea": true,
    cursors: {
      template: '<div class="custom-cursor">...</div>',
      hideDelayMs: 5000,
      hideSpeedMs: 0,
      selectionChangeSource: null,
      transformOnTextChange: true,
    },
    focus: {
      focusClass: "focus-blot",
    },
  };

  const formats = [
    "header",
    "bold",
    "italic",
    "underline",
    "list",
    "bullet",
    "link",
    "image",
    "emoji",
    "strike",
    "formula",
    "table",
  ];

  return (
    <div style={{ padding: "20px", margin: "auto" }}>
      <ReactQuill
        style={{ height: "300px", width: "600px" }}
        theme="snow"
        ref={(el) => {
          reactQuillRef = el;
        }}
        value={value.text}
        onChange={(value) => {
          setValue({ text: value });
        }}
        modules={modulesRef}
        formats={formats}
        placeholder="Enter the text here"
      />
    </div>
  );
}

export default RichTextEditor;
